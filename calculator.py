import kivy
kivy.require("1.9.0")

import __future__

from kivy.app import App
from kivy.uix.gridlayout import GridLayout
 
class CalculatorGridLayout(GridLayout):

    def calculate(self, calculation):
        if calculation:
            try:
                self.display.text = str(eval(compile(calculation, '<string>', 'eval', __future__.division.compiler_flag)))
            except Exception:
                self.display.text = "Error"
 
class CalculatorApp(App):
 
    def build(self):
        return CalculatorGridLayout()
 
calcApp = CalculatorApp()
calcApp.run()