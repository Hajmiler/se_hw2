# Kalkulator
## Autor: Ivan Hajmiler

## Upute:

Aplikacija pruža četiri osnovne računske operacije:  
    + (zbrajanje)  
    - (oduzimanje)  
    * (množenje)  
    / (dijeljenje)  

Kada želimo računati s decimalnim brojevima koristimo dugme "." (decimalna točka).

Pritiskom na dugme "=" dobivamo rješenje unesene računske operacije, a pritiskom na dugme "AC" brišemo trenutnu računicu i omogućujemo unos nove.